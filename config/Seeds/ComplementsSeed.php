<?php
use Migrations\AbstractSeed;

/**
 * Complements seed.
 */
class ComplementsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');

        $table = $this->table('complements');

        foreach (range(1, 30) as $productId) {
            foreach (range(1, 3) as $times) {
                $priceRules = [];

                foreach (range(0, 4) as $priceRule) {
                    $priceRules[] = [
                        'minSize' => $priceRule * 1,
                        'maxSize' => ($priceRule * 1) + 0.999,
                        'price' => ($priceRule + 1) * 40.35
                    ];
                }

                $extras = $faker->randomElement([
                    'LADO DIREITO,LADO ESQUERDO',
                    'LADO DIREITO',
                    ''  
                ]);

                $category = '';
                if (!empty($extras)) {
                    $category = $faker->randomElement([
                        'SUPORTES',
                        'SETAS',
                        'PORTA'
                    ]);
                }

                $data = [
                    'image_id' => $faker->numberBetween(1, 10),
                    'product_id' => $productId,
                    'name' => $faker->sentence(3, true),
                    'price_rules' => json_encode($priceRules),
                    'extras' => $extras,
                    'category' => $category,
                    'created' => date('y-m-d H:i:s'),
                    'modified' => date('y-m-d H:i:s')
                ];
                $table->insert($data);
            }
        }

        $table->save();
    }
}
