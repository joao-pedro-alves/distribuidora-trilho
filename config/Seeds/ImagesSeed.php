<?php
use Migrations\AbstractSeed;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Image;

/**
 * Images seed.
 */
class ImagesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');

        $table = $this->table('images');

        for ($p=1; $p<=30; $p++) {
            for ($i=0; $i<3; $i++) {
                $data = [
                    'item_id' => $p,
                    'type' => Image::TYPE_PRODUCT,
                    'name' => $faker->randomElement([
                        'tmp1.jpg',
                        'tmp2.jpg',
                        'tmp3.jpg',
                        'tmp4.jpg'
                    ]),
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ];

                if ($i === 0) {
                    $data += ['thumb' => 1];
                }
                $table->insert($data);
            }
        }

        $table->save();
    }
}
