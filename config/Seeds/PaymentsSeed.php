<?php
use Migrations\AbstractSeed;

/**
 * Payments seed.
 */
class PaymentsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');
        $table = $this->table('payments');

        foreach (range(1, 30) as $paymentId) {
            $userId = $paymentId;

            $data = [
                'user_id' => $userId,
                'amount' => $faker->randomFloat(2, 45.35, 384.5),
                'status' => $faker->randomElement([0, 1, 1, 1, 1, 1, 2, 2, 2]),
                'created' => date('y-m-d H:i:s'),
                'modified' => date('y-m-d H:i:s')
            ];

            $table->insert($data);
        }

        $table->save();
    }
}
