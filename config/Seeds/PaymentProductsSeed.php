<?php
use Migrations\AbstractSeed;

/**
 * PaymentProducts seed.
 */
class PaymentProductsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        $table = $this->table('payment_products');
        $faker = Faker\Factory::create('pt_BR');

        foreach (range(1, 30) as $paymentId) {
            foreach (range(1, 3) as $productId) {
                $data = [
                    'payment_id' => $paymentId,
                    'product_id' => $faker->numberBetween(1, 30),
                    'size' => $faker->randomFloat(3, 0.500, 3.999),
                    'amount' => $faker->randomFloat(2, 10.5, 150.5),
                    'created' => date('y-m-d H:i:s'),
                    'modified' => date('y-m-d H:i:s')
                ];

                $table->insert($data);
            }
        }

        $table->save();
    }
}
