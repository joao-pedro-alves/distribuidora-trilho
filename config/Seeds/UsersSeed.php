<?php
use Migrations\AbstractSeed;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $table = $this->table('users');
        $faker = Faker\Factory::create('pt_BR');
        
        $data = [
            'first_name' => 'Administrador',
            'last_name' => 'Alves',
            'role' => 1,
            'status' => 1,
            'zipcode' => '25710-310',
            'document' => $faker->randomElement([$faker->cpf(false), $faker->cnpj(false)]),
            'address_street' => 'Rua Professor Spartaco Banal',
            'address_number' => '191',
            'address_neighborhood' => 'Itamarati',
            'address_city' => 'Petrópolis',
            'address_state' => 'RJ',
            'phone' => '(24) 9924-441306',
            'email' => 'admin@mail.com',
            'password' => (new DefaultPasswordHasher)->hash('qweqwe'),
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ];
        $table->insert($data);

        for ($i=0; $i < 50; $i++) {
            $table->insert([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'role' => 0,
                'status' => $faker->randomElement([1, 1, 1, 1, 0]),
                'zipcode' => $faker->postcode,
                'document' => $faker->randomElement([$faker->cpf(false), $faker->cnpj(false)]),
                'address_street' => $faker->streetName,
                'address_number' => $faker->buildingNumber,
                'address_neighborhood' => $faker->secondaryAddress,
                'address_city' => $faker->city,
                'address_state' => $faker->stateAbbr,
                'phone' => $faker->cellphoneNumber,
                'email' => $faker->email,
                'password' => (new DefaultPasswordHasher)->hash('qweqwe'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ]);
        }
        $table->save();
    }
}
