<?php
use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $table = $this->table('categories');
        $faker = Faker\Factory::create('pt_BR');

        for ($i=0; $i<5; $i++) {
            $table->insert([
                'name' => 'Categoria #' . $i,
                'created' => date('y-m-d H:i:s'),
                'modified' => date('y-m-d H:i:s')
            ]);
        }

        for ($i=0; $i<15; $i++) {
            $table->insert([
                'parent_id' => rand(1, 5),
                'name' => 'Sub-Categoria #' . $i,
                'created' => date('y-m-d H:i:s'),
                'modified' => date('y-m-d H:i:s')
            ]);
        }

        $table->save();
    }
}
