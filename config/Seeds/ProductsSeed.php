<?php
use Migrations\AbstractSeed;

/**
 * Products seed.
 */
class ProductsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');
        $table = $this->table('products');
        
        for ($i=0; $i<30; $i++) {
            $priceRules = [];
            foreach (range(0, 4) as $priceRule) {
                $priceRules[] = [
                    'minSize' => $priceRule * 1,
                    'maxSize' => ($priceRule * 1) + 0.999,
                    'price' => ($priceRule + 1) * 70.35
                ];
            }

            $table->insert([
                'name' => $faker->sentence(3, true),
                'description' => $faker->sentence(6, true),
                'category_id' => $faker->numberBetween(6, 19),
                'price_rules' => json_encode($priceRules),
                'created' => date('y-m-d H:i:s'),
                'modified' => date('y-m-d H:i:s')
            ]);
        }
        $table->save();
    }
}
