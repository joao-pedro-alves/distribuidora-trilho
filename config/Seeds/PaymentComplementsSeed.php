<?php
use Migrations\AbstractSeed;

/**
 * PaymentComplements seed.
 */
class PaymentComplementsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');
        $table = $this->table('payment_complements');

        foreach (range(1, 90) as $productId) {
            foreach (range(1, 3) as $a) {
                $data = [
                    'payment_product_id' => $productId,
                    'complement_id' => $faker->numberBetween(1, 3),
                    'size' => $faker->randomFloat(3, 0.500, 3.999),
                    'amount' => $faker->randomFloat(2, 10.5, 150.5),
                    'extras' => $faker->randomElement([
                        'LADO DIREITO',
                        'LADO ESQUERDO',
                        ''  
                    ]),
                    'created' => date('y-m-d H:i:s'),
                    'modified' => date('y-m-d H:i:s')
                ];
                $table->insert($data);
            }
        }

        $table->save();
    }
}
