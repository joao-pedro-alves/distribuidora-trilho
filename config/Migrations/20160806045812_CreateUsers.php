<?php
use Migrations\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('first_name', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('last_name', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('document', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('role', 'integer', [
            'default' => 0,
            'null' => false
        ]);
        $table->addColumn('status', 'integer', [
            'default' => 0,
            'null' => false
        ]);
        $table->addColumn('zipcode', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('address_street', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('address_number', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('address_neighborhood', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('address_city', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('address_state', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('phone', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('password', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
