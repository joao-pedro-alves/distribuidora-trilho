<?php
use Migrations\AbstractMigration;

class CreateProducts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('products');
        $table->addColumn('category_id', 'integer', [
            'default' => null,
            'limit' => 11
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('description', 'string', [
            'default' => null,
            'limit' => 255
        ]);
        $table->addColumn('price_rules', 'string', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null
        ]);
        $table->create();
    }
}
