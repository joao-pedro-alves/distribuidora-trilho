<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentProductsTable Test Case
 */
class PaymentProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentProductsTable
     */
    public $PaymentProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payment_products',
        'app.payments',
        'app.users',
        'app.complements',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentProducts') ? [] : ['className' => 'App\Model\Table\PaymentProductsTable'];
        $this->PaymentProducts = TableRegistry::get('PaymentProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
