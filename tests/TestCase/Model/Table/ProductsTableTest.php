<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Validation\Validator;

/**
 * App\Model\Table\ProductsTable Test Case
 */
class ProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductsTable
     */
    public $Products;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = ['app.products'];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Products = TableRegistry::get('Products');
    }

    public function testValidationDefault()
    {   
        // campo ID
        $data = ['id' => ''];
        $product = $this->Products->newEntity($data);
        $this->assertEmpty($product->errors('id'));

        $tmpEntity = $this->Products
            ->newEntity(['name' => 'test'], ['validate' => null]);
        $tmpEntity->isNew(false);

        $product = $this->Products->patchEntity($tmpEntity, $data);
        $this->assertNotEmpty($product->errors('id'));

        $tmpEntity->id = 1;
        $product = $this->Products->patchEntity($tmpEntity, []);
        $this->assertEmpty($product->errors('id'));

        // campo NAME
        $product = $this->Products->newEntity([]);
        $this->assertArrayHasKey('_required', $product->errors('name'));

        $product = $this->Products->newEntity(['name' => '']);
        $this->assertArrayHasKey('_empty', $product->errors('name'));

        $product = $this->Products->newEntity(['name' => ' a test name']);
        $this->assertEmpty($product->errors('name'));

        // campo DESCRIPTION
        $product = $this->Products->newEntity([]);
        $this->assertArrayHasKey('_required', $product->errors('description'));

        $product = $this->Products->newEntity(['description' => '']);
        $this->assertArrayHasKey('_empty', $product->errors('description'));

        $product = $this->Products->newEntity(['description' => 'a teste description']);
        $this->assertEmpty($product->errors('description'));

        // campo PRICE
        $product = $this->Products->newEntity([]);
        $this->assertArrayHasKey('_required', $product->errors('price'));

        $product = $this->Products->newEntity(['price' => '']);
        $this->assertArrayHasKey('_empty', $product->errors('price'));

        $product = $this->Products->newEntity(['price' => 'R$ 14.34']);
        $this->assertEmpty($product->errors('price'));
    }

}
