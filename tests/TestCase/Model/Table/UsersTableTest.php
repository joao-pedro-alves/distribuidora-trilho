<?php
namespace App\Test\TestCase\Model\Table;

use \App\Model\Entity\User;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Validation\Validator;


/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    public $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Users = TableRegistry::get('Users');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationCreate()
    {
        $validator = $this->Users->validationCreate(new Validator);
        $validateFields = $validator->getIterator();

    }

    public function testValidationEdit()
    {
        // Testando presença do ID
        $user = $this->Users->newEntity([
        ], [
            'validate' => 'edit'
        ]);
        $this->assertNotEmpty($user->errors('id'));

        $user = $this->Users->newEntity([
            'id' => ''
        ], [
            'validate' => 'edit'
        ]);
        $this->assertNotEmpty($user->errors('id'));

        // Testando campo email
        $user = $this->Users->newEntity([
            'email' => 'test@mail.com'
        ]);
        $this->Users->save($user);

        $this->Users->patchEntity($user, [], [
            'validate' => false
        ]);
        $this->Users->save($user, [
            'validate' => 'edit'
        ]);
        
        $this->assertEmpty($user->errors('email'));

        // Testando campos requiridos
        $data = [
            'first_name' => 'João Pedro',
            'last_name' => 'Alves de Souza',
            'zipcode' => '25710-310',
            'address_street' => 'Rua Professor Spartaco Banal',
            'address_number' => '191',
            'address_neighborhood' => 'Itamarati',
            'address_city' => 'Petrópolis',
            'address_state' => 'RJ',
            'document' => '16306136767',
            'phone' => '(24) 9924-441306',
            'email' => 'joaopedroalv9@gmail.com',
            'password' => 'qweqwe',
            'password_confirm' => 'qweqwe'
        ];
        foreach ($data as $field => $value) {
            $user = $this->Users->newEntity([$field => ''] + $data, [
                'validate' => 'edit'
            ]);
            $this->assertNotEmpty($user->errors());
        }

        // Testando inserção de sucesso
        $user = $this->Users->newEntity($data);
        $save = $this->Users->save($user);
        $this->assertNotNull($save);

        $data['id'] = 2;
        $user = $this->Users->patchEntity($save, $data, ['validate' => 'edit']);
        $this->assertFalse($user->isNew());
        $save = $this->Users->save($user, ['validate' => 'edit']);
        $this->assertNotFalse($save);
    }

    public function testGetByStatus()
    {
        $data = [
            [
                'id' => 1,
                'status' => User::STATUS_PENDING,
                'email' => 'test1@mail.com'
            ],
            [
                'id' => 2,
                'status' => User::STATUS_APPROVED,
                'email' => 'test2@mail.com'
            ]
        ];
        $entities = $this->Users->newEntities($data);
        foreach ($entities as $entity) $this->Users->save($entity);

        $users = $this->Users->getByStatus(User::STATUS_PENDING);
        $this->assertEquals(1, $users->count());

        $users = $this->Users->getByStatus(User::STATUS_APPROVED);
        $this->assertEquals(1, $users->count());

        $users = $this->Users->getByStatus([
            User::STATUS_APPROVED,
            User::STATUS_PENDING
        ]);
        $this->assertEquals(2, $users->count());

        $users = $this->Users->getByStatus(99999);
        $this->assertEquals(0, $users->count());
    }

    public function testSetUserStatus()
    {
        $data = [
            'email' => 'test@mail.com',
            'status' => User::STATUS_DISAPPROVED
        ];
        $entity = $this->Users->newENtity($data);
        $this->Users->save($entity);

        $this->assertEquals(User::STATUS_DISAPPROVED, $entity->status);

        $attempt = $this->Users->setStatus(-1, User::STATUS_APPROVED);
        $this->assertNull($attempt);

        $this->Users->setStatus($entity->id, User::STATUS_APPROVED);

        $user = $this->Users->findById($entity->id)->first();
        $this->assertEquals(User::STATUS_APPROVED, $user->status);
    }

    public function testDeleteById()
    {
        $this->assertFalse($this->Users->deleteById(-1));

        $user = $this->Users->newEntity(['email' => 'test@mail.com']);
        $user = $this->Users->save($user);
        $this->assertNotNull($user);

        $this->assertNotFalse($this->Users->deleteById($user->id));
        $this->assertNull($this->Users->findById($user->id)->first());
    }
}
