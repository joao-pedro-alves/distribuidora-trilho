<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\CategoryHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\CategoryHelper Test Case
 */
class CategoryHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\CategoryHelper
     */
    public $Category;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Category = new CategoryHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Category);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
