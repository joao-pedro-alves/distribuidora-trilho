<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\SearchComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\SearchComponent Test Case
 */
class SearchComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\SearchComponent
     */
    public $Search;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Search = new SearchComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Search);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
