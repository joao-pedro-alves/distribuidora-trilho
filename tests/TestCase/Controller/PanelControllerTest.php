<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PanelController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\PanelController Test Case
 */
class PanelControllerTest extends IntegrationTestCase
{
	public $fixtures = [
	    'app.users'
	];

	public function testDashboardDeveEstarAcessivel()
	{
		$this->get('/panel/dashboard');
		$this->assertResponseSuccess();
	}

	public function testDashboardDeveRedirecionarUsuarioSemAcesso()
	{
		$this->get('/panel/dashboard');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login']);

		$this->session([
		    'Auth' => [
		        'User' => [
		            'id' => 1,
		            'email' => 'e@mail.com',
		            'role' => 0
		        ]
		    ]
		]);
		$this->get('/panel/dashboard');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login'], 'Usuários sem nível de permissão não estão sendo redirecionados');

		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get('/panel/dashboard');
		$this->assertResponseOK();
	}

	public function testUsersManagerDeveEstarAcessivel()
	{
		$this->post(['controller' => 'Panel', 'action' => 'usersManager']);
		$this->assertResponseSuccess();
	}

	public function testUsersManagerDeveRedirecionarUsuarioSemAcesso()
	{
		$this->post(['controller' => 'Panel', 'action' => 'usersManager']);
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login']);


		$this->session(['Auth' => ['User' => ['role' => 0]]]);
		$this->post(['controller' => 'Panel', 'action' => 'usersManager']);
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login']);
	}

	public function testUsersManagerDevePermitirAcessoUsuarioSemAcesso()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'usersManager']);
		$this->assertResponseOk();
	}

	public function testUserEditDeveEstarAcessivel()
	{
		$this->get(['controller' => 'Panel', 'action' => 'userEdit']);
		$this->assertResponseSuccess();
	}

	public function testUserEditDeveRedirecionarIdInvalido()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userEdit', -1]);
		$this->assertRedirect(['controller' => 'Panel', 'action' => 'dashboard']);
	}

	public function testeUserEditDeveEstarOkUsuarioValido()
	{
		$User = TableRegistry::get('Users');
		$entity = $User->newEntity([
			'email' => 'test@mail.com'
		]);
		$User->save($entity);

		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userEdit', 1]);
		$this->assertResponseOk();
	}

	public function testUserViewDeveEstarAcessivel()
	{
		$Users = TableRegistry::get('Users');
		$user = $Users->newEntity(['email' => 'test@mail.com']);
		$Users->save($user);
		$this->assertNotNull($user);


		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userView', 1]);
		$this->assertResponseOK();
		
	}

	public function testUserViewUsuarioInvalidoDeveRedirecionar()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userView', -1]);
		$this->assertRedirect();
	}

	public function testUserApproveDeveEstarAcessivelERedirecionando()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userApprove', 1]);
		$this->assertRedirect();
	}

	public function testUserApproveDeveAprovarUsuario()
	{
		$Users = TableRegistry::get('Users');
		$user = $Users->newEntity(['email' => 'test@mail.com']);
		$user = $Users->save($user);
		$this->assertNotNull($user);
		$this->assertEquals(0, $user->status);
		$this->assertEquals(1, $user->id);

		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userApprove', 1]);
		$user = $Users->findById(1)->first();
		$this->assertNotNull($user);
		$this->assertEquals(\App\Model\Entity\User::STATUS_APPROVED, $user->status);
	}

	public function testUserDisapproveDeveEstarAcessivelERedirecionando()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userDisapprove', 1]);
		$this->assertRedirect();
	}	

	public function testUserDisapproveDeveReprovarUsuario()
	{
		$Users = TableRegistry::get('Users');
		$user = $Users->newEntity(['email' => 'test@mail.com']);
		$user = $Users->save($user);
		$this->assertNotNull($user);
		$this->assertEquals(0, $user->status);
		$this->assertEquals(1, $user->id);

		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userDisapprove', 1]);
		$user = $Users->findById(1)->first();
		$this->assertNotNull($user);
		$this->assertEquals(\App\Model\Entity\User::STATUS_DISAPPROVED, $user->status);
	}

	public function testUserDeleteDeveEStarAcessivelERedirecionando()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userDelete', 1]);
		$this->assertRedirect();
	}

	public function testUserDeleteDeveDeletarUsuario()
	{
		$Users = TableRegistry::get('Users');
		$user = $Users->newEntity(['email' => 'test@mail.com']);
		$user = $Users->save($user);
		$this->assertNotNull($user);

		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'userDelete', $user->id]);

		$this->assertEquals(0, $Users->find()->count());
	}

	public function testProductsManagerDeveEstarAcessivel()
	{
		$this->session(['Auth' => ['User' => ['role' => 1]]]);
		$this->get(['controller' => 'Panel', 'action' => 'productsManager']);
		$this->assertResponseOk();
	}
}
