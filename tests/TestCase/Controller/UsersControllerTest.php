<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;
use App\Model\Entity\User;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    public function testRegisterDeveEstarAcessivel()
    {
        $this->get('/users/register');
        $this->assertResponseOK();
    }

    public function testRegisterDeveRedirecionarLogado()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'e@mail.com'
                ]
            ]
        ]);

        $this->get('/users/register');
        $this->assertRedirect('/');        
    }

    public function testDeveRegistrarUsuario()
    {
        $data = [
            'first_name' => 'João Pedro',
            'last_name' => 'Alves de Souza',
            'zipcode' => '25710-310',
            'address_street' => 'Rua Professor Spartaco Banal',
            'address_number' => '191',
            'address_neighborhood' => 'Itamarati',
            'address_city' => 'Petrópolis',
            'address_state' => 'RJ',
            'document' => '16396136767',
            'phone' => '(24) 9924-441306',
            'email' => 'joaopedroalv9@gmail.com',
            'password' => 'qweqwe',
            'password_confirm' => 'qweqwe'
        ];

        $this->post('/users/register', $data);
        $this->assertRedirect([
            'controller' => 'Users',
            'action' => 'registerSuccess'
        ]);
        $this->assertCookieEncrypted(
            $data['email'],
            'registerSuccessEmail',
            'aes',
            null,
            "Não está sendo salvo o email de registro no cookie"
        );

        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['email' => 'joaopedroalv9@gmail.com']);
        $this->assertEquals(1, $query->count());
    }

    public function testRegisterSuccessDeveEstarAcessivel()
    {
        $this->get('/users/register-success');
        $this->assertResponseSuccess();
    }

    public function testRegisterSuccessDeveRedirecionarRequisicaoSemCookie()
    {
        $this->get('/users/register-success');
        $this->assertRedirect('/', 'Não está redirecionando requisições sem o cookie de email');
    }

    public function testRegisterSuccessDeveDarAcessoPaginaRequisicaoComCookie()
    {
        $this->cookieEncrypted('registerSuccessEmail', 'myemail@mail.com');
        $this->get('/users/register-success');
        $this->assertResponseOK();
    }

    public function testRegisterSuccessDeveRenderizarViewCorreta()
    {
        $this->cookieEncrypted('registerSuccessEmail', 'myemail@mail.com');
        $this->get('/users/register-success');
        $this->assertTemplate('register_success');
    }

    public function testRegisterSuccessDevePassaEmailMostrarNaView()
    {
        $this->cookieEncrypted('registerSuccessEmail', 'myemail@mail.com');
        $this->get('/users/register-success');

        $this->assertEquals('myemail@mail.com', $this->viewVariable('email'));
        $this->assertResponseContains('myemail@mail.com');
    }

    public function testLoginDeveEstarAcessivel()
    {
        $this->get('/users/login');
        $this->assertResponseOK();
    }

    public function testLoginDeveFalhar()
    {
        $data = [
            'email' => 'mywrongemail@mail.com',
            'password' => 'mypass'
        ];

        $this->post('/users/login', $data);
        $this->assertRedirect([
            'controller' => 'users',
            'action' => 'login',
            '?' => [
                'unauthorized' => 1
            ]
        ]);
    }

    public function testLoginDeveLogar()
    {
        $Users = TableRegistry::get('Users');
        $data = [
            'email' => 'works@mail.com',
            'password' => 'qweqwe',
            'status' => User::STATUS_APPROVED
        ];
        $entity = $Users->newEntity($data);
        $Users->save($entity);

        $this->post('/users/login', $data);
        $this->assertRedirect([
            'controller' => 'Site',
            'action' => 'dashboard'
        ]);

        $this->assertSession('works@mail.com', 'Auth.User.email');
    }

    public function testLoginDeveRedirecionarLogados()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'e@mail.com'
                ]
            ]
        ]);

        $this->get('/users/login');
        $this->assertRedirect('/');
    }
}
