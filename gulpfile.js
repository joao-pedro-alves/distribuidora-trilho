var gulp = require('gulp');
var merge = require('merge2');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');

gulp.task('sass', function() {
	return gulp.src(['src/Assets/scss/*.scss', 'src/Assets/scss/**/*.scss'])
		.pipe(sass())
		.pipe(cleanCSS())
		.pipe(concat('app.css'))
		.pipe(gulp.dest('webroot/css'));
});

gulp.task('compress-js', function() {
	var general = gulp.src([
		'src/Assets/js/components/*.js',
		'src/Assets/js/jquery-plugins/*.js'
	]);

	var angular = gulp.src([
		'src/Assets/js/angular/app.js',
		'src/Assets/js/angular/filters/*.js',
		'src/Assets/js/angular/directives/*.js',
		'src/Assets/js/angular/directives/**/*.js'
	]);

	merge(general, angular)
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(uglify())
		.pipe(concat('app.js'))
		.pipe(gulp.dest('webroot/js'));
});

gulp.task('default', ['sass', 'compress-js']);

gulp.watch([
	'src/Assets/scss/*.scss',
	'src/Assets/scss/**/*.scss'
], ['sass']);

gulp.watch([
	'src/Assets/js/*.js',
	'src/Assets/js/components/*.js',
	'src/Assets/js/jquery-plugin/*.js',
	'src/Assets/js/angular/filters/*.js',
	'src/Assets/js/angular/directives/*.js',
	'src/Assets/js/angular/**/*.js'
], ['compress-js']);
