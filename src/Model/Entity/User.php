<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $zipcode
 * @property string $address_street
 * @property string $address_number
 * @property string $address_neighborhood
 * @property string $address_city
 * @property string $address_state
 * @property string $phone
 * @property string $email
 * @property string $password
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    /**
     *
     **/
    protected $_virtual = ['full_name'];

    /**
     * Estado das atualizações
     **/
    const STATUS_PENDING        = 0;
    const STATUS_APPROVED       = 1;
    const STATUS_DISAPPROVED     = 2;

    /**
     *
     **/
    public function _getFullName()
    {
        return $this->first_name . ' ' .
            $this->last_name;
    }

    /**
     *
     **/
    public function _getStatusName()
    {
        if ($this->status == static::STATUS_DISAPPROVED) {
            return 'Desaprovado';
        }

        if ($this->status == static::STATUS_APPROVED) {
            return 'Aprovado';
        }

        if ($this->status == static::STATUS_PENDING) {
            return 'Pendente';
        }

    }
}
