<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $description
 * @property float $price
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Category $category
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = [
        'price_money_format',
        'thumb',
        'gallery'
    ];

    /**
     *
     **/
    public function _getPriceMoneyFormat()
    {
        return sprintf("R$%.2f", $this->price);
    }

    /**
     *
     **/
    public function _getThumb()
    {
        if (isset($this->images)) {
            foreach ($this->images as $image) {
                if ($image->thumb == true) {
                    return $image->name;
                }
            }

            return null;
        }
    }

    /**
     *
     **/
    public function _getGallery()
    {
        $gallery = [];
        if (!isset($this->images)) return;
        foreach ($this->images as $image) {
            if ($image->thumb != true) {
                $gallery[] = $image;
            }
        }

        return $gallery;
    }
}
