<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentCategories
 * @property \Cake\ORM\Association\HasMany $ChildCategories
 * @property \Cake\ORM\Association\HasMany $Products
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('categories');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ParentCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Products', [
            'foreignKey' => 'category_id'
        ]);
    }

    /*8
     *
     **/
    public function getAllParents()
    {
        $categories = $this->find()
            ->where(['parent_id' => 0])
            ->contain(['ChildCategories']);

        return $categories;
    }

    /**
     *
     **/
    public function getAllOptionFormat()
    {
        $categories = $this->getAllParents()->toArray();

        $optionFormat = [];
        foreach ($categories as $category) {
            $optionFormat[$category->name] = [];

            foreach ($category->child_categories as $child) {
                $optionFormat[$category->name][$child->id] = $child->name;
            }
        }

        return $optionFormat;
    }

    /**
     *
     **/
    public function getParentsOptionFormat()
    {
        $categories = $this->find()
            ->where(['parent_id' => 0])
            ->contain(['ChildCategories'])
            ->toArray();

        $optionFormat = [];
        foreach ($categories as $category) {
            $optionFormat[$category->id] = $category->name;
        }

        return $optionFormat;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'Informe o nome da categoria');

        $validator
            ->requirePresence('parent_id')
            ->allowEmpty('parent_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['parent_id'], 'ParentCategories'));
        $rules->add(
            $rules->isUnique(['name'], 'Já existe uma categoria com este nome')
        );

        return $rules;
    }
}
