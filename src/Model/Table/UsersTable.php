<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 */
class UsersTable extends Table
{
    const ERROR_MSG_EMPTY = "Este campo é obrigatório";
    const ERROR_MSG_INVALID_VALUE = "Valor informado inválido";
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     *
     **/
    public function getByStatus($status)
    {
        if (is_array($status)) {
            $query = $this->find();
            foreach ($status as $whereCondition) {
                $query->orWhere(['status' => $whereCondition]);
            }
        } else {
            $query = $this->findByStatus($status);
        }
        return $query;
    }

    /**
     *
     **/
    public function setStatus($userId, $status)
    {
        $entity = $this->findById($userId)->first();

        if (!$entity) {
            return null;
        }

        $entity = $this->patchEntity($entity, ['status' => $status]);
        $this->save($entity);
    }

    /**
     *
     **/
    public function beforeSave(Event $event, $entity)
    {
        $entity->password = (new DefaultPasswordHasher())->hash($entity->password);
    }

    /**
     *
     **/ 
    public function validationCreate(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->requirePresence('first_name')
            ->notEmpty('first_name', self::ERROR_MSG_EMPTY)
            ->lengthBetween('first_name', [3, 32], self::ERROR_MSG_INVALID_VALUE);

        $validator
            ->requirePresence('last_name')
            ->notEmpty('last_name', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('document')
            ->notEmpty('document', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('zipcode')
            ->notEmpty('zipcode', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('address_street')
            ->notEmpty('address_street', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('address_number')
            ->notEmpty('address_number', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('address_neighborhood')
            ->notEmpty('address_neighborhood', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('address_city')
            ->notEmpty('address_city', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('address_state')
            ->notEmpty('address_state', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('phone')
            ->notEmpty('phone', self::ERROR_MSG_EMPTY);

        $validator
            ->email('email')
            ->requirePresence('email')
            ->notEmpty('email', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('password')
            ->notEmpty('password', self::ERROR_MSG_EMPTY);

        $validator
            ->requirePresence('password_confirm')
            ->notEmpty('password_confirm', self::ERROR_MSG_EMPTY)
            ->add('password_confirm', 'no-misspelling', [
                'rule' => ['compareWith', 'password'],
                'message' => 'As senhas não são iguais'
            ]);

        return $validator;
    }

    public function validationEdit(Validator $validator)
    {
        $validator = $this->validationCreate($validator);

        $validator
            ->requirePresence('id')
            ->integer('id')
            ->notEmpty('id');        

        return $validator;
    }


    /**
     *
     **/
    public function deleteById($userId = 0)
    {
        $user = $this->findById($userId)->first();

        if ($user === null) {
            return false;
        }

        $this->delete($user);
    }

    /**
     *
     **/
    public function buildRules(RulesChecker $rules)
    {
        $rules->add(
            $rules->isUnique(['email'], 'Este email já está cadastrado')
        );

        return $rules;
    }
}
