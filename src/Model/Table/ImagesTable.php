<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\Image;

/**
 * Images Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Items
 *
 * @method \App\Model\Entity\Image get($primaryKey, $options = [])
 * @method \App\Model\Entity\Image newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Image[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Image|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Image patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Image[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Image findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ImagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('images');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->belongsTo('Items', [
        //     'className' => ''
        //     'foreignKey' => 'item_id'
        // ]);
    }

    /**
     *
     **/
    public function handleRequestUpload($image, $type, $thumb)
    {
        $imgName = $this->randomImageName(pathinfo($image['name'], PATHINFO_EXTENSION));
        $this->moveUpload($imgName, $image['tmp_name']);
        $imageEntity = $this->add([
            'name' => $imgName,
            'type' => ($type == 'product') ? Image::TYPE_PRODUCT : Image::TYPE_COMPLEMENT,
            'thumb' => (bool)$thumb
        ]);

        return [
            'image_name' => $imgName,
            'image_id' => $imageEntity->id
        ];
    }

    /**
     *
     **/
    public function add($data)
    {
        $imageEntity = $this->newEntity($data);
        if ($this->save($imageEntity)) {
            return $imageEntity;
        }

        return false;
    }

    /**
     *
     **/
    public function getUploadPath($imgName)
    {
        return  WWW_ROOT . 'uploads' . DS . $imgName;
    }

    /**
     *
     **/
    public function unlinkByName($imgName)
    {
        unlink($this->getUploadPath($imgName));
    }

    /**
     *
     **/
    public function moveUpload($imgName, $imgTmpName)
    {
        $imgDest = $this->getUploadPath($imgName);
        move_uploaded_file($imgTmpName, $imgDest);
        chmod($imgDest, 0666);
    }

    /**
     *
     **/
    public function randomImageName($ext = null)
    {
        $name = md5(uniqid(time(), true));
        if ($ext !== null) {
            $name .= "." . $ext;
        }
        return $name;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->boolean('thumb')
            ->requirePresence('thumb', 'create')
            ->notEmpty('thumb');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
       return $rules;
    }
}
