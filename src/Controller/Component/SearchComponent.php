<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Search component
 */
class SearchComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     *
     **/
    public function handle($query, $options = [])
    {
    	$options = $options + [
    		'query_key' => 'q',
    		'search_fields' => []
    	];

    	$inputSearch = $this->request->query($options['query_key']);
    	if ($inputSearch) {
    		$searchWords = explode(' ', $inputSearch);
    		$searchFields = $options['search_fields'];
    
    		$filter = [];
    		foreach ($searchFields as $field) {
    			foreach ($searchWords as $word) {
    				$filter[] = [$field . ' LIKE' => '%'. $word .'%'];
    			}
    		}

    		$query->where(['OR'=> $filter]);
    	}
    }
}
