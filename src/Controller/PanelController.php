<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Entity\User;

/**
 * Panel Controller
 *
 * @property \App\Model\Table\PanelTable $Panel
 */
class PanelController extends AppController
{
	// /**
	//  *
	//  **/
	// public $paginate = [
	// 	'limit' => 3,
	// 	'order' => [
	// 		'Users.id' => 'desc'
	// 	]
	// ];

	/**
	 *
	 **/
	public function initialize()
	{
		parent::initialize();

		$this->loadComponent('Paginator');
		$this->loadModel('Users');
	}

	/**
	 *
	 **/
	public function dashboard()
	{
	}

	/**
	 *
	 **/
	public function usersManager()
	{
		$this->loadComponent('Search');

		$usersPending = $this->Users->getByStatus(User::STATUS_PENDING);
		$usersAllowed = $this->Users->getByStatus([
			User::STATUS_APPROVED,
			User::STATUS_DISAPPROVED
		]);

		$this->Search->handle($usersAllowed, [
			'search_fields' => [
				'Users.first_name',
				'Users.last_name',
				'Users.email',
				'Users.phone'
			]
		]);

		$usersAllowed = $this->paginate($usersAllowed);

		$this->set(compact('usersAllowed', 'usersPending'));
	}

	/**
	 *
	 **/
	public function userEdit($userId = 0)
	{
		$user = $this->Users->findById($userId)->first();	

		if (!$user) {
			return $this->redirect(['controller' => 'Panel', 'action' => 'dashboard']);
		}

		if ($this->request->is('put')) {
			$userEntity = $this->Users->patchEntity($user, $this->request->data);
			if ($this->Users->save($userEntity, ['validate' => 'edit'])) {
				return $this->redirect([
					'controller' => 'Panel',
					'action' => 'userEdit',
					'?' => [
						'status' => 'success'
					],
					$userId
				]);
			}
		}

		$user->password = '';
		$this->set(compact('user'));
	}

	/**
	 *
	 **/
	public function userView($userId = 0)
	{
		$user = $this->Users->findById($userId)->first();

		if (!$user) {
			return $this->redirect(['controller' => 'Panel', 'action' => 'dashboard']);
		}

		$this->set(compact('user'));
	}

	/**
	 * 
	 **/
	public function userApprove($userId = 0)
	{
		$this->Users->setStatus($userId, User::STATUS_APPROVED);
		return $this->redirect($this->referer());
	}

	/**
	 *
	 **/
	public function userDisapprove($userId = 0)
	{
		$this->Users->setStatus($userId, User::STATUS_DISAPPROVED);
		return $this->redirect($this->referer());	
	}

	/**
	 *
	 **/
	public function userDelete($userId = 0)
	{
		$user = $this->Users->findById($userId)->first();

		if (!$user) {
			return $this->redirect(['controller' => 'Panel', 'action' => 'usersManager']);
		}

		$this->Users->delete($user);
		return $this->redirect(['controller' => 'Panel', 'action' => 'usersManager', 'status' => 'user-deleted']);
	}

	/**
	 *
	 **/
	public function productsManager()
	{
		$this->loadModel('Products');
		$this->loadComponent('Search');

		$productsQuery = $this->Products->find()
			->contain(['Categories', 'Images']);

		$this->Search->handle($productsQuery, [
			'search_fields' => ['Products.name']
		]);

		$products = $this->paginate($productsQuery);

		$this->set(compact('products')); 
	}

	/**
	 *
	 **/
	public function productAdd()
	{
		$this->loadModel('Categories');
		$categoriesOption = $this->Categories->getAllOptionFormat();
		$categoriesParent = $this->Categories->getParentsOptionFormat();

		$this->set(compact('categoriesOption', 'categoriesParent'));
	}

	/**
	 *
	 **/
	public function categoriesManager()
	{
		$this->loadModel('Categories');
		$categories = $this->Categories->getAllParents();

		$this->set(compact('categories'));
	}

	/**
	 *
	 **/
	public function categoryAdd()
	{
		$this->loadModel('Categories');

		$categoryEntity = $this->Categories->newEntity();

		if ($this->request->is('post')) {
			$this->Categories->patchEntity($categoryEntity, $this->request->data);
			if ($this->Categories->save($categoryEntity)) {
				return $this->redirect([
					'controller' => 'Panel',
					'action' => 'categoriesManager',
					'?' => [
						'action' => 'add',
						'status' => 'success'
					]
				]);
			}
		}

		$categoriesOption = $this->Categories->getParentsOptionFormat();

		$this->set(compact('categoriesOption', 'categoryEntity'));
	}

	/**
	 *
	 **/
	public function categoryEdit($categoryId = 0)
	{
		$this->loadModel('Categories');

		$categoriesOption = $this->Categories->getParentsOptionFormat();
		$categoryEntity = $this->Categories->findById($categoryId)->first();
		if (empty($categoryEntity)) {
			return $this->redirect(['controller' => 'Panel', 'action' => 'categoriesManager']);
		}

		if ($this->request->is(['post', 'put'])) {
			$this->Categories->patchEntity($categoryEntity, $this->request->data);
			if ($this->Categories->save($categoryEntity)) {
				return $this->redirect([
					'controller' => 'Panel',
					'action' => 'categoriesManager',
					'?' => [
						'action' => 'edit',
						'status' => 'success'
					]
				]);
			}
		}

		$this->set(compact('categoriesOption', 'categoryEntity'));

		$this->render('category_add');	
	}

	/**
	 *
	 **/
	public function categoryDelete($categoryId = 0)
	{
		$this->loadModel('Categories');

		$categoryEntity = $this->Categories
			->findById($categoryId)
			->contain(['ChildCategories'])
			->first();

		if (empty($categoryEntity)) {
			return $this->redirect(['controller' => 'Panel', 'action' => 'categoriesManager']);
		}

		foreach ($categoryEntity->child_categories as $child) {
			$child->parent_id = 0;
			$this->Categories->save($child);
		}

		$this->Categories->delete($categoryEntity);
		return $this->redirect([
			'controller' => 'Panel',
			'action' => 'categoriesManager',
			'?' => [
				'action' => 'delete',
				'status' => 'success'
			]
		]);
	}

	/**
	 *
	 **/
	public function paymentsManager()
	{
		$this->loadModel('Payments');

		$paymentsPendingEntity = $this->Payments->getPendings();
		$paymentsPendingEntity->contain(['Users']);

		$paymentsActiveEntity = $this->Payments->getActives();
		$paymentsActiveEntity->contain(['Users']);
		$paymentsActiveEntity = $this->Paginator->paginate($paymentsActiveEntity, ['limit' => 10]);

		$this->set(compact('paymentsPendingEntity', 'paymentsActiveEntity'));
	}

	/**
	 *
	 **/
	public function paymentView($paymentId = 0)
	{
		$this->loadModel('Payments');

		$paymentEntity = $this->Payments
			->findById($paymentId)
			->contain([
				'Users',
				'Items',
				'Items.Product',
				'Items.Complements', 
				'Items.Complements.Complement'
			])
			->first();

		if (!$paymentEntity) {
			return $this->redirect([
				'controller' => 'Panel',
				'action' => 'paymentsManager'
			]);
		}

		$this->set(compact('paymentEntity'));
	}

	/**
	 *
	 **/
	public function paymentSet($paymentId, $status)
	{
		$this->loadModel('Payments');

		$paymentEntity = $this->Payments->findById($paymentId)->first();
		if (!$paymentEntity) {
			return $this->redirect([
				'controller' => 'Panel',
				'action' => 'paymentsManager'
			]);	
		}

		$paymentEntity->status = $status;
		$this->Payments->save($paymentEntity);

		return $this->redirect([
			'controller' => 'Panel',
			'action' => 'paymentsManager',
			'?' => [
				'action' => 'status',
				'status' => 'success' 
			]
		]);	
	}
	
	/**
	 *
	 **/
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		if (!$this->Auth->user('role')) {
			return $this->redirect(['controller' => 'Users', 'action' => 'login']);
		}
	}
}
