<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\ServiceUnavailableException;
use Cake\Event\Event;
use Cake\Routing\Router;
use Exception;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class ServicesController extends AppController
{
    private $_token = 'Lorem ipsum dolor sit amet';

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
    /**
     *
     **/    
    public function categoryAdd()
    {
        $this->loadModel('Categories');

        $name = $this->request->data('name');
        $parentId = $this->request->data('parent_id');


        if ($name === NULL || $parentId === NULL) {
            throw new ServiceUnavailableException();
        }

        $categoryVerifyExistsQuery = $this->Categories->findByName($name);
        if ($categoryVerifyExistsQuery->count()) {
            $this->set('_body', [
                'exists' => true
            ]);
        } else {
            $category = $this->Categories->newEntity($this->request->data);
            $this->Categories->save($category);

            $this->set('_body', [
                'id' => $category->id
            ]);
        }
    }

    /**
     *
     **/
    public function imageUpload()
    {
        $this->loadModel('Images');

        try {
            $image = $this->request->data('file');
            if (!$image) {
                throw new Exception("Requisicao inválida");
            }

            $imageUpload = $this->Images->handleRequestUpload(
                $image,
                $this->request->data('type'),
                $this->request->data('thumb')
            );

            $this->set('_body', [
                'success' => true,
                'url' => Router::url('/uploads/' . $imageUpload['image_name'], true),
                'image_id' => $imageUpload['image_id']
            ]);
        } catch (Exception $e) {
            $this->set('_body', [
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }

    public function imageDelete()
    {
        $this->loadModel('Images');

        $imgId = $this->request->data('id');
        if (!$imgId) {
            throw new NotFoundException();
        }

        try {
            $imageEntity = $this->Images->findById($imgId)->first();
            if (!$imageEntity) {
                throw new Exception("Imagem não existe");
            }

            $this->Images->unlinkByName($imageEntity->name);
            $this->Images->delete($imageEntity);

            $this->set('_body', [
                'success' => true
            ]);
        } catch (Exception $e) {
            $this->set('_body', [
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->RequestHandler->renderAs($this, 'json');
        $this->set('_serialize', '_body');

        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }

        if ($this->request->data('token') != $this->_token) {
            throw new ServiceUnavailableException();
        }

        $this->Auth->allow();
    }
}
