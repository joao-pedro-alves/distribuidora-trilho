<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Entity\User;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	public function login()
	{
		if ($this->Auth->user()) {
			return $this->redirect('/');
		}

		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				if ($user['status'] != User::STATUS_APPROVED) {
					return $this->redirect(['controller' => 'Users', 'action' => 'login', 'status' => 'denied']);
				}

				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			} else {
				return $this->redirect([
					'controller' => 'users',
					'action' => 'login',
					'?' => [
						'unauthorized' => 1
					]
				]);
			}
		}
	}

	public function logout()
	{
		$this->redirect($this->Auth->logout());
	}

	public function register()
	{
		if ($this->Auth->user()) {
			return $this->redirect('/');
		}
		
		$user = $this->Users->newEntity();

		if ($this->request->is('post')) {
			$this->Users->patchEntity($user, $this->request->data, [
				'validate' => 'create'
			]);
			if ($this->Users->save($user, ['validate' => 'create'])) {
				$this->Cookie->write('registerSuccessEmail', $user->email);
				return $this->redirect([
					'controller' => 'users',
					'action' => 'registerSuccess'
				]);
			} else {
				$this->set('formErrors', true);
			}
		}

		$this->set([
			'userEntity' => $user
		]);
	}

	public function registerSuccess()
	{
		$userEmail = $this->Cookie->read('registerSuccessEmail');

		if (!$userEmail) {
			return $this->redirect('/');
		}

		$this->set(['email' => $userEmail]);
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		$this->Auth->allow('register');
		$this->Auth->allow('registerSuccess');
	}
}
