(function() {
	var selectDOM, inputDOM, errorDOM, selectParentDOM, submitDOM;

	$(function() {
		selectDOM = $("#product-add-category-select");
		inputDOM = $("#product-add-category-input");
		errorDOM = $("#product-add-category-input-error");
		selectParentDOM = $("#product-add-category-select-parent");
		submitDOM = $("#product-add-category-submit");

		bindUIActions();
	});

	var bindUIActions = function() {
		submitDOM.on('click', function(e) {
			e.preventDefault();

			if (!inputDOM.val().length) {
				setErrorMessage('Informe o nome da categoria');
				return;
			}

			if (inputDOM.val().length < 3) {
				setErrorMessage('O nome informado é muito pequeno');
				return;
			}

			clearErrors();

			requestAddCategory();
		})
	};

	var requestAddCategory = function() {
		var parentId = selectParentDOM.val();
		var name = inputDOM.val();

		$.post(BASEURL + 'services/category-add', {
			token:  REQUEST_TOKEN,
			name: name,
			parent_id: parentId
		}, function(response) {
			clearErrors();
			clearInputDOM();

			if (response['exists'] !== undefined) {
				setErrorMessage('Esta categoria já existe');
				return;
			}

			if (parentId == 0) {
				addItemSelectParent(response.id, name);
			} else {
				addItemSelect(parentId, response.id, name);
				selectDOM.val(response.id);
			}

			selectParentDOM.val('0');
		});
	};

	var addItemSelect = function(parentId, itemId, name)
	{
		var parentName = selectParentDOM.find('[value="'+ parentId +'"]').text();
		var optContainer = selectDOM.find('[label="'+ parentName +'"]');
		var newOption = $("<option />")
			.text(name)
			.val(itemId);

		optContainer.append(newOption);
	};

	var addItemSelectParent = function(itemId, name)
	{
		var newOption = $("<option />")
			.text(name)
			.attr('value', itemId);
		selectParentDOM.append(newOption);

		var newOptGroup = $("<optgroup />")
			.attr('label', name);
		selectDOM.append(newOptGroup);
	};

	var setErrorMessage = function(message) {
		errorDOM.text(message);
	};

	var clearErrors = function() {
		errorDOM.empty();
	};

	var clearInputDOM = function() {
		inputDOM.val('');
	};
})();