'use strict';

(function() {
	const NAMESPACE = 'upload-image-simple';

	var Factory = {
		run: function() {
			$("[data-"+ NAMESPACE +"]").each(function() {
				new ImageUploadSimple($(this), $(this).data(NAMESPACE));
			});
		}
	};

	var ImageUploadSimple = function(containerDOM, options) {
		console.log(options);

		this.options = options;
		this.el = {};
		this.el.container = containerDOM;
		this.loading = false;

		this.mappingElements();
		this.initSimpleUploadPlugin();
		this.loadInputData();
	};

	ImageUploadSimple.prototype.mappingElements = function() {
		this.el.submitBtn = this.el.container.find('[upload-submit]');
		this.el.image = this.el.container.find('[upload-image]');
		this.el.inputData = this.el.container.find('[upload-input-data]');
	};

	ImageUploadSimple.prototype.setThumb = function(imgUrl) {
		this.el.image
			.css('background-image', 'url('+ imgUrl +')')
			.show();
	};

	ImageUploadSimple.prototype.setInputData = function(imgId, imgUrl) {
		var content = {
			'image_url': imgUrl,
			'image_id': imgId
		};

		this.el.inputData.val(JSON.stringify(content));
		this.el.inputData.trigger('input');
	};

	ImageUploadSimple.prototype.loadInputData = function() {
		var content = this.el.inputData.val();

		if (content) {
			content = JSON.parse(content);
			this.setThumb(content.image_url);
		}
	};

	ImageUploadSimple.prototype.setLoading = function(status) {
		if (status === true) {
			this.loading = true;
			this.el.submitBtn.text('Carregando...');
		} else {
			this.loading = false;
			this.el.submitBtn.text('Carregar imagem');
		}
	};

	ImageUploadSimple.prototype.initSimpleUploadPlugin = function() {
		var self = this;

		new ss.SimpleUpload({
			button: this.el.submitBtn,
			url: BASEURL + 'services/image-upload',
			name: 'file',
			multiple: false,
			allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
			onSubmit: function() {
				this.setData({
					token: REQUEST_TOKEN,
					type: self.options.type,
					thumb: true
				});

				if (self.loading === true) {
					return false;
				}

				self.setLoading(true);
			},
			onComplete: function(file, response) {
				response = JSON.parse(response);
				self.setThumb(response.url);
				self.setInputData(response.image_id, response.url);
				self.setLoading(false);
			}
		});
	};

	// Runs the factory
	$(function() {
		Factory.run();
	});
})();