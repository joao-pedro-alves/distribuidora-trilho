'use strict';

(function() {
	var el = {};

	var mappingElements = function() {
		el.submitBtn = $("#product-upload-gallery-submit");
		el.inputData = $("#product-upload-gallery-input-data");
		el.imagesWrap = $("#product-upload-gallery-images-wrap");
	};

	var loadInputData = function() {
		var content = el.inputData.val();

		if (content) {
			content = JSON.parse(content);

			for (var imgId in content) {
				addImage(imgId, content[imgId]);
			}
		}
	};

	var removeImage = function(imgId) {
		var content = JSON.parse(el.inputData.val());
		delete content[imgId];
		el.inputData.val(JSON.stringify(content));
	};

	var addImage = function(imgId, imgUrl) {
		var html = '<div class="col-md-3"> \
						<div class="image-box"> \
							<div class="img-wrap"></div> \
							<div class="text-center"> \
								<button type="button" class="btn btn-xs btn-danger">Remover</button> \
							</div> \
						</div> \
					</div>';

		var element = $("<div />").html(html);
		element.find('.img-wrap')
			.css('background-image', 'url('+ imgUrl +')');

		element.find('button')
			.on('click', function(e) {
				e.preventDefault();

				$.post(BASEURL + 'services/image-delete', {
					token: REQUEST_TOKEN,
					id: imgId
				}, function(response) {
					removeImage(imgId);
					element.fadeOut('slow', function() {
						$(this).remove();
					});
				});
			});
	
		el.imagesWrap.prepend(element);
	};

	var updateInputData = function(imgId, imgUrl) {
		var content = {};
		if (el.inputData.val()) {
			content = JSON.parse(el.inputData.val());
		}

		content[imgId] = imgUrl;

		el.inputData.val(JSON.stringify(content));
	};

	var initSimpleUploadPlugin = function() {
		new ss.SimpleUpload({
			button: el.submitBtn,
			url: BASEURL + 'services/image-upload',
			name: 'file',
			multiple: true,
			allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
			onSubmit: function() {
				this.setData({
					token: REQUEST_TOKEN,
					type: 'product',
					thumb: false
				});
			},
			onComplete: function(file, response) {
				var response = JSON.parse(response);
				addImage(response.image_id, response.url);
				updateInputData(response.image_id, response.url);
			}
		});
	};

	$(function() {
		mappingElements();
		initSimpleUploadPlugin();
		loadInputData();
	});
})();