	// this.el.name = $("#product-manager-complement-name");
	// this.el.loadImageBtn = $("#product-manager-complement-load-image");
	// this.el.imageWrap = $("#product-manager-complement-image-wrap");
	// this.el.rulesAlert = $("#product-manager-complement-rules-alert");

var ComplementsManagerComponent = {
	el: {},

	run: function() {
		this.bindAddPriceRulesUIActions();
	},

	bindAddPriceRulesUIActions: function() {
		var self = this;

		this.el.minSize = $("#product-manager-size-min");
		this.el.maxSize = $("#product-manager-size-max");
		this.el.price = $("#product-manager-complement-price");
		this.el.addRuleBtn = $("#product-manager-complement-add-rule-btn");

		this.el.addRuleBtn.on('click', function(e) {
			e.preventDefault;

			if (!self.validatePriceRule()) {
				return;
			}

			var data = self.parseRuleData();
			console.log(data);
		});
	},

	parseRuleData: function() {
		var minSize = this.el.minSize.val();
		var maxSize = this.el.maxSize.val();
		var price = this.el.price.val();

		return {
			'minSize': parseFloat(minSize.replace('m', '').replace(',', '.')),
			'maxSize': parseFloat(maxSize.replace('m', '').replace(',', '.')),
			'price': parseFloat(price.replace(',', '.'))
		};
	},

	validatePriceRule: function() {
		var minSize = this.el.minSize.val();
		var maxSize = this.el.maxSize.val();
		var price = this.el.price.val();

		if (!minSize || !maxSize || !price) {
			return false;
		}

		return true;
	}
};

var ComplementsComponent = function() {
	this.name = '';
	this.image = {};
	this.priceRules = [];
};

var PriceRuleComponent = function() {
	this.active = true;
	this.min = 0;
	this.max = 0;
	this.price = 0.0;
	this.html = '\
		<td> \
			<button type="button" class="btn btn-xs btn-danger">Remover</button> \
		</td> \
		<td>0m</td> \
		<td>1,000m</td> \
		<td>R$33,40</td> \
	';
												
};

// Inicializando
ComplementsManagerComponent.run();