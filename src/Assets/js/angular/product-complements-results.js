angular.module('panel')
	.directive('productComplementsResults', () => {
		var _fakeData = [
			{
				name: 'Nome de teste #1',
				image: {image_id: 1, image_url: 'http://localhost/trilho/uploads/4b44ff25ffcc871678eb6d0141ef23bd.jpg'},
				'priceRules': [
					{minVal: 0, maxVal: 0.999, amount: 10.5},
					{minVal: 1, maxVal: 1.999, amount: 20.55},
					{minVal: 2, maxVal: 2.999, amount: 30.87}
				]
			},
			{
				name: 'Nome de teste #2',
				image: {image_id: 1, image_url: 'http://localhost/trilho/uploads/4b44ff25ffcc871678eb6d0141ef23bd.jpg'},
				'priceRules': [
					{minVal: 0, maxVal: 0.999, amount: 10.5},
					{minVal: 1, maxVal: 1.999, amount: 20.55},
					{minVal: 2, maxVal: 2.999, amount: 30.87}
				]
			}
		];


		var _controller = ['$scope', ($scope) => {
			$scope.complements = _fakeData;

			$scope.requestEdit = (index) => {
				$scope.edit({
					index: index
				});
			}
		}];

		return {
			restrict: 'E',
			scope: {
				edit: '&onEdit'
			},
			controller: _controller,
			template: `
				<table class="table body-middle">
					<thead>
						<tr>
							<th></th>
							<th width="50"></th>
							<th>Nome</th>
							<th>Regras de valor</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="complement in complements">
							<td>
								<button ng-click="edit({index: $index})" type="button" class="btn btn-xs btn-default">Editar</button>
							</td>
							<td>
								<img width="40" height="40" src="http://localhost/trilho/uploads/4b44ff25ffcc871678eb6d0141ef23bd.jpg" />
							</td>
							<td>{{complement.name}}</td>
							<td>
								<div ng-repeat="priceRule in complement.priceRules">
									<b>{{priceRule.minVal | meters}}m</b> até <b>{{priceRule.maxVal | meters}}m</b> - <b>R{{priceRule.amount | currency}}</b>
								</div>
							</td>
						</tr>
					</tbody>
				</table>`
		}
	});