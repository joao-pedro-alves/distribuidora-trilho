angular.module('panel')
	.directive('productComplementCategories', () => {
		return {
			restrict: 'E',
			scope: {
				'categories': '=',
				'delete': '&onDelete',
				'edit': '&onEdit',
				'addComplement': '&onAddComplement'
			},
			template: `
				<div class="panel panel-default" ng-repeat="(category, items) in categories">
					<div class="panel-heading">
						<strong><i class="fa fa-star"></i> {{category}}</strong>
						<div class="pull-right">
							<button ng-click="addComplement({category, category})" title="Adicionar novo opcional" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button>
							<button ng-click="delete({name: category})" title="Remover esta categoria" type="button" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></button>
						</div>
					</div>
					<table class="table body-middle">
						<tbody>
							<tr ng-repeat="item in items" product-complement-item
								item="item"
								on-delete="deleteItem(category, $index)"
								on-edit="edit()"
							></tr>
						</tbody>
					</table>
				</div>	
			`
		}
	});