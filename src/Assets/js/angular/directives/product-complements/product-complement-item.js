angular.module('panel')
	.directive('productComplementItem', () => {
		return {
			restrict: 'AE',
			scope: {
				'item': '=',
				'delete': '&onDelete',
				'edit': '&onEdit'
			},
			template: `
				<td width="70">
					<button ng-click="edit({data: item})" title="Editar este opcional" type="button" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button>
					<button ng-click="delete()" title="Remover este opcional" type="button" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></button>
				</td>
				<td width="50">
					<img width="40" height="40" src="{{item.image.image_url}}" />
				</td>
				<td width="170">
					<strong>{{item.name}}</strong>
				</td>
				<td>
					<div ng-repeat="rule in item.priceRules">
						<b>{{rule.minVal | meters}}m</b> até <b>{{rule.maxVal | meters}}m</b> - <b>R{{rule.price | currency}}</b>
					</div>
				</td>
			`
		}
	});