angular.module('panel')
	.directive('productComplement', () => {
		var _controller = ['$scope', ($scope) => {
			$scope.addMode = false;

			$scope.complements = [
				{
					"name": "Nome do meu complemento",
					"category": "PEDESTAIS",
					"image": {"image_id": 1, "image_url": "http://localhost/trilho/uploads/c5f345a3c87a10203226bdc06b81d892.jpg"},
					"extras": "LADO DIREITO,LADO ESQUERDO",
					"priceRules": [
						{"minVal": 0, "maxVal": 0.999, "price": 10.5},
						{"minVal": 1, "maxVal": 1.999, "price": 20.5},
						{"minVal": 2, "maxVal": 2.999, "price": 30.5}
					]
				}
			];

			/**
			 *
			 **/
			 $scope.toggleAddMode = () => {
			 	$scope.addMode = !$scope.addMode;
			 };

			 /**
			  *
			  **/
			 $scope.addComplement = (category) => {
			 	$scope.toggleAddMode();
			 	$scope.editCategoryName = category;
			 };

			/**
			 *
			 **/
			 $scope.editComplement = (data) => {
			 	alert( data );
			 };

			/**
			 *
			 **/
			$scope.addCategory = (name) => {
				$scope.categories[name.toUpperCase()] = $scope.categories[name] || [];
			};

			/**
			 *
			 **/
			 $scope.deleteCategory = (name) => {
			 	let confirm = window.confirm('Tem certeza que deseja DELETAR esta categoria e todos os seus items?');

			 	if (confirm) {
			 		delete $scope.categories[name];
			 	}
			 };

			 /**
			  *
			  **/
			 $scope.deleteItem = (category, index) => {
			 	let confirm = window.confirm('Tem certeza que deseja DELETAR este opcional?');

			 	if (confirm) {
				 	$scope.categories[category].splice(index, 1);
			 	}
			 };

			/**
			 *
			 **/
			$scope.$watch('complements', (a, b) => {
				// Definindo categorias
				$scope.categories = {};
				$scope.complements.map((item) => {
					let categoryName = item.category;
					
					if (typeof $scope.categories[categoryName] === 'undefined') {
						$scope.categories[categoryName] = [];
					}

					$scope.categories[categoryName].push(item);
				});
			}, true);
		}];

		return {
			restrict: 'E',
			controller: _controller,
			template: `
				<div ng-show="!addMode">
					<product-complement-add-category
						on-add="addCategory(name)"
					/>
					<product-complement-categories
						categories="categories"
						on-delete="deleteCategory(name)"
						on-edit="editComplement($data)"
						on-add-complement="addComplement(category)"
					/>
				</div>
				<product-complement-add-form ng-show="addMode"
					on-back-categories="toggleAddMode()"
					category="editCategoryName"
				/>
			`,
		}
	});