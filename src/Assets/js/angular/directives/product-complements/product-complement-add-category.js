angular.module('panel')
	.directive('productComplementAddCategory', () => {
		var _controller = ['$scope', ($scope) => {
			$scope.clearInput = () => $scope.name = '';
		}];

		return {
			restrict: 'E',
			controller: _controller,
			scope: {
				'requestAdd': '&onAdd'
			},
			template: `
				<div class="form-group text-right">
					<div class="input-group">
						<input ng-model="name" type="text" class="form-control" placeholder="Nome da categoria">
						<span class="input-group-btn">
						<button ng-click="requestAdd({name: name}); clearInput()" type="button" class="btn btn-default"><i class="fa fa-star"></i> Nova categoria</button>
					</div>
				</div>
			`
		}
	});