angular.module('panel')
	.directive('productComplementAddForm', () => {
		var _addComplement = (index) => {
			alert('ok!!! ' + index);
		};

		var controller = ['$scope', ($scope) => {
			$scope.optionals = [];
			$scope.current = {
				name: '',
				image: {},
				priceRules: [],
				extras: ''
			};

			$scope.$watch('current.extras', (a, b) => {
				console.log('Mudou', a, b);
			});

			$scope.addComplement = _addComplement;
		}];

		return {
			restrict: 'E',
			scope: {
				'toggleAddMode': '&onBackCategories',
				'category': '='
			},
			controller: controller,
			template: `
				<div class="form-wrap" data-upload-image-simple='{"type": "product"}'>
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-left">
								<strong>Categoria: <i>{{category}}</i></strong>
							</div>
							<div class="text-right">
								<button ng-click="toggleAddMode()" type="button" class="btn btn-sm btn-default">Voltar</button>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="pic" upload-image></div>
							<div class="text-center">
								<button upload-submit type="button" class="btn btn-sm btn-primary">Carregar imagem</button>
								<input ng-model="current.image" upload-input-data type="hidden" />
							</div>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<label for="nome">Nome</label>
								<input ng-model="current.name" type="text" class="form-control" />
							</div>
							<div class="form-group">
								<label>Extras</label>
								<input tags-input ng-model="current.extras" type="text" />
							</div>
							<price-rules-manager input-handle="current-optional-price-rule-data">
							</price-rules-manager>
							<input ng-model="current.priceRules" id="current-optional-price-rule-data" type="hidden" />
							<div class="form-group">
								<button ng-click="addComplement()" type="button" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Adicionar opcional</button>
							</div>
						</div>
					</div>
					<!-- Lista de complementos -->
					<div class="row">
						<div class="col-md-12">
							<product-complements-results
								on-edit="addComplement(index)"
							>
							</product-complements-results>
						</div>
					</div>
					<!-- Lista de complementos --
				</div>
			`
		}
	});