angular.module('plugins')
	.directive('tagsInput', () => {
		var _link = (scope, element) => {
			$(element).tagsInput({
				width: '100%',
				height: '80px',
				defaultText: 'Adicionar',
				onChange: (selector) => {
					$(selector).trigger('input');
				}
			});
		};

		return {
			restrict: 'A',
			scope: {},
			link: _link
		}
	});