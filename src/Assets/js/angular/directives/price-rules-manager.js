angular.module('panel')
	.directive('priceRulesManager', () => {
		var link = (scope, elem, attrs) => {
			var inputHandle = angular.element(document.getElementById(attrs.inputHandle));
			
			scope.$watch((newScope) => {
				if (typeof newScope.dom.amount !== 'undefined') {
					scope.currentRule.amount = newScope.dom.amount.toString();
					scope.currentRule.amount = parseFloat(scope.currentRule.amount.replace('.', '').replace(',', '.'));
				}

				if (typeof newScope.dom.minVal !== 'undefined') {
					newScope.currentRule.minVal = newScope.dom.minVal.toString();
					newScope.currentRule.minVal = parseFloat(newScope.currentRule.minVal.replace('m', '').replace(',', '.'));
				}

				if (typeof newScope.dom.maxVal !== 'undefined') {
					newScope.currentRule.maxVal = newScope.dom.maxVal.toString();
					newScope.currentRule.maxVal = parseFloat(newScope.currentRule.maxVal.replace('m', '').replace(',', '.'));
				}


				inputHandle.val(angular.toJson(scope.rules));
				inputHandle.trigger('input');
			});
		};

		var controller = ['$scope', ($scope) => {
			$scope.rules = [];

			$scope.dom = {};

			$scope.currentRule = {
				minVal: 0,
				maxVal: 0,
				amount: 0
			};

			$scope.addRule = () => {
				if (!$scope.currentRule.minVal || !$scope.currentRule.maxVal || !$scope.currentRule.amount) {
					return;
				}

				$scope.rules.push({
					minVal: $scope.currentRule.minVal,
					maxVal: $scope.currentRule.maxVal,
					amount: $scope.currentRule.amount
				});

				$scope.currentRule = {
					minVal: 0,
					maxVal: 0,
					amount: 0
				};

				$scope.dom = {};
			};

			$scope.removeRule = (index) => {
				$scope.rules.splice(index, 1);
			};
		}];

		return {
			restrict: 'E',
			'scope': {},
			'link': link,
			'controller': controller,
			'template': `
				<label>Regra de valor</label>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-5">
							<input ng-model="dom.minVal" type="text" class="form-control" input-mask-meter />
						</div>
						<div class="col-sm-2 text-center">
							<label>até</label>
						</div>
						<div class="col-sm-5">
							<input ng-model="dom.maxVal" type="text" class="form-control" input-mask-meter />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Valor</label>
					<div class="input-group">
						<div class="input-group-addon">R$</div>
						<input ng-model="dom.amount" type="text" class="form-control" input-mask-money />
					</div>
				</div>
				<button ng-click="addRule()" type="button" class="btn btn-sm btn-primary">Adicionar regra</button>
				<hr />
				<!-- Lista de regras de valores -->
				<table class="table table-striped" id="product-manager-complement-rules-table">
					<thead>
						<tr>
							<th></th>
							<th>Mínimo</th>
							<th>Máximo</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="rule in rules">
							<td>
								<button ng-click="removeRule($index)" type="button" class="btn btn-xs btn-danger">Remover</button>
							</td>
							<td>{{rule.minVal.toFixed(3).toString().replace('.', ',')}}m</td>
							<td>{{rule.maxVal.toFixed(3).toString().replace('.', ',')}}m</td>
							<td>R$ {{rule.amount.toFixed(2).toString().replace('.', ',')}}</td>
						</tr>
					</tbody>
				</table>
				<!-- Lista de regras de valores -->
			`
		}
	});
