angular.module('panel')
	.filter('meters', () => {
		var _factory = (input) => {
			var val = parseFloat(input).toFixed(3);
			val = val.toString().replace('.', ',');
			return val;
		};

		return _factory;
	});