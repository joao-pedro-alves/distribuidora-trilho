$(function() {
	$("[input-mask-meter]")
		.mask("990,000m", {reverse: true, placeholder: '0,000m', clearIfNotMatch: true});

	$("[input-mask-money]")
		.mask("#.##0,00", {reverse: true, placeholder: '0,00', clearIfNotMatch: true});
});