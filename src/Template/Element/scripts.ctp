<?= $this->Html->script('../vendor/js/jquery-3.1.0.min.js')  ?>
<?= $this->Html->script('../vendor/js/bootstrap.min.js')  ?>
<?= $this->Html->script('../vendor/js/featherlight.js')  ?>
<?= $this->Html->script('../vendor/js/jquery.fine-uploader.min.js')  ?>
<?= $this->Html->script('../vendor/js/SimpleAjaxUploader.min.js')  ?>
<?= $this->Html->script('../vendor/js/cidades-estados-1.4-utf8.js')  ?>
<?= $this->Html->script('../vendor/js/jquery.mask.min.js')  ?>
<?= $this->Html->script('../vendor/js/jquery.tagsinput.min.js')  ?>
<?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js')  ?>
<script>
	const BASEURL = '<?= $this->Url->build('/', true) ?>';
	const REQUEST_TOKEN = 'Lorem ipsum dolor sit amet';
</script>
<?= $this->Html->script('app.js')  ?>