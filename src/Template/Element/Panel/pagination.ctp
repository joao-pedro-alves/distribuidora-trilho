<nav class="text-center">
	<ul class="pagination">
		<?= $this->Paginator->numbers([
			'first' => 'First page', 'url' => ['#' => isset($hashtag) ? $hashtag : '']
		]) ?>	
	</ul>
</nav>