<?= $this->Form->create(
	null, [
		'type' => 'get',
		'url' => ['#' => 'approved-users']
	]
) ?>
<div class="form-group has-success">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-search"></i></span>
		<?= $this->Form->input('q', [
			'label' => false,
			'class' => 'form-control input-lg',
			'placeholder' => $placeholder,
			'autocomplete' => 'off',
			'value' => $this->request->query('q'),
			'templates' => [
				'inputContainer' => '{{content}}'
			]

		]) ?>
	</div>
</div>
<?= $this->Form->end() ?>