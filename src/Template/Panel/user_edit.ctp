<?php $this->layout('panel') ?>

<section id="page-user-edit">
	<h1 class="main-page-title">Editando usuário</h1>
	<div class="content-padding-wrap">
	<!-- Mensagem de sucesso -->
	<?php if ($this->request->query('status') == 'success'): ?>
	<div class="alert alert-success">
		<span><i class="fa fa-check"></i> Usuário editado com sucesso</span>
	</div>
	<?php endif ?>
	<!-- Mensagem de sucesso -->
	<?= $this->Form->create($user) ?>
	<div class="row">
		<div class="col-sm-6">
			<?= $this->Form->input('first_name', [
				'label' => 'Nome',
				'class' => 'form-control form-group'
			]) ?>		
		</div>
		<div class="col-sm-6">
			<?= $this->Form->input('last_name', [
				'label' => 'Sobrenome',
				'class' => 'form-control form-group'
			]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $this->Form->input('zipcode', [
				'label' => 'CEP',
				'class' => 'form-control form-group'
			]) ?>
			<?= $this->Form->input('address_street', [
				'label' => 'Endereço',
				'class' => 'form-control form-group'
			]) ?>	
			<?= $this->Form->input('address_number', [
				'label' => 'Número',
				'class' => 'form-control form-group'
			]) ?>	
		</div>
		<div class="col-sm-6">
			<?= $this->Form->input('address_neighborhood', [
				'label' => 'Bairro',
				'class' => 'form-control form-group'
			]) ?>
			<?= $this->Form->input('address_state', [
				'label' => 'Estado',
				'class' => 'form-control form-group'
			]) ?>
			<?= $this->Form->input('address_city', [
				'label' => 'Cidade',
				'class' => 'form-control form-group'
			]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $this->Form->input('email', [
				'label' => 'Email',
				'class' => 'form-control form-group'
			]) ?>	
		</div>
		<div class="col-sm-6">
			<?= $this->Form->input('phone', [
				'label' => 'Telefone',
				'class' => 'form-control form-group'
			]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $this->Form->input('password', [
				'type' => 'password',
				'label' => 'Senha',
				'class' => 'form-control form-group'
			]) ?>
		</div>
		<div class="col-sm-6">
			<?= $this->Form->input('status', [
				'class' => 'form-control form-group',
				'options' => [
					'0' => 'Aguardando aprovação',
					'1' => 'Aprovado',
					'2' => 'Reprovado'
				]	
			]) ?>
		</div>
	</div>
	<div class="form-group">
		<button class="btn btn-success">Salvar</button>
		<div class="pull-right">
			<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userDelete', $user->id]) ?>" class="btn btn-danger" onclick="return confirm('Tem certeza que deseja excluir PERMANENTEMENTE este cadastro?')"><i class="fa fa-trash"></i> Excluir</a>
		</div>
	</div>
	<?= $this->Form->end() ?>
	</div>
</section>