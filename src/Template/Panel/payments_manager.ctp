<?php 
	$this->layout('panel');
	$this->assign('crumb-panel', 'payments');
?>

<section id="page-product-add">
	<h1 class="main-page-title"><i class="fa fa-money"></i> Faturas</h1>
	<div class="content-padding-wrap">
		<!-- Mensagem de status -->
		<?php if ($this->request->query('action') == 'status' && $this->request->query('status') == 'success'): ?>
		<div class="alert alert-success">
			<span><i class="fa fa-check"></i> <b>Fatura editada com sucesso!</b></span>
		</div>
		<?php endif ?>
		<!-- Mensagem de status -->
		<!-- Tabela de faturas pendentes -->
		<div class="panel panel-info">
			<div class="panel-heading"><span><i class="fa fa-clock-o"></i> <b>Existem <?= $paymentsPendingEntity->count() ?> orçamentos aguardando aprovação</b></span></div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Cliente</th>
						<th>Status</th>
						<th>Valor</th>
						<th>Emissão</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($paymentsPendingEntity as $payment): ?>
					<tr class="warning">
						<td>
							<div class="btn-group">
								<button 
									class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-cogs"></i> Opções <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentView', $payment->id]) ?>" ><i class="fa fa-eye"></i> Visualizar</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentSet', $payment->id, 1]) ?>"><span class="text-success"><i class="fa fa-check"></i> Marcar como paga</span></a>
									</li>
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentSet', $payment->id, 2]) ?>"><span class="text-danger"><i class="fa fa-close"></i> Marcar como cancelada</span></a
									</li>
								</ul>
							</div>
						</td>
						<td><?= $payment->user->full_name ?></td>
						<td>
							<div class="span label label-xs label-warning">Pendente</div>
						</td>
						<td><?= $payment->amount_cash ?></td>
						<td><?= $payment->created->format('d/m/Y') ?></td>
					</tr>
					<?php endforeach ?>
					<?php foreach ($paymentsActiveEntity as $payment): ?>
					<tr>
						<td>
							<div class="btn-group">
								<button 
									class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-cogs"></i> Opções <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentView', $payment->id]) ?>" ><i class="fa fa-eye"></i> Visualizar</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentSet', $payment->id, 1]) ?>"><span class="text-success"><i class="fa fa-check"></i> Marcar como paga</span></a>
									</li>
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentSet', $payment->id, 2]) ?>"><span class="text-danger"><i class="fa fa-close"></i> Marcar como cancelada</span></a
									</li>
								</ul>
							</div>
						</td>
						<td><?= $payment->user->full_name ?></td>
						<td>
							<?php if ($payment->status == 1): ?>
							<span class="label label-xs label-success">Paga</span>
							<?php else: ?>
							<span class="label label-xs label-danger">Cancelada</span>
							<?php endif ?>
						</td>
						<td><?= $payment->amount_cash ?></td>
						<td><?= $payment->created->format('d/m/Y') ?></td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<!-- Paginação -->
			<?= $this->Element('Panel/pagination') ?>
			<!-- Paginação -->
		</div>
		<!-- Tabela de faturas pendentes -->
	</div>
</section>