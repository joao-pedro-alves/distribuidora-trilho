<?php 
	$this->layout('panel');
	$this->assign('crumb-panel', 'products');
?>

<section id="page-product-add">
	<h1 class="main-page-title">Adicionando produto</h1>
	<div class="content-padding-wrap">
		<?= $this->Form->create() ?>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<?= $this->Form->input('name', [
						'label' => 'Nome',
						'class' => 'form-control'
					]) ?>
				</div>
				<div class="form-group">
					<?= $this->Form->input('description', [
						'type' => 'textarea',
						'label' => 'Descrição',
						'class' => 'form-control'
					]) ?>
				</div>
				<div class="form-group">
					<div class="panel panel-info">
						<div class="panel panel-heading"><b>Regras de valor</b></div>
						<div class="panel panel-body">
							<price-rules-manager input-handle="loucura">
							</price-rules-manager>
						</div>
					</div>
					<?= $this->Form->input('algoo', [
						'type' => 'hidden',
						'id' => 'loucura',
						'label' => false,
						'class' => 'form-control'
					]) ?>
				</div>
				<hr />
				<!-- Galeria de imagens -->
				<div class="panel panel-info" id="product-gallery-wrap">
					<div class="panel-heading"><b>Galeria</b></div>
					<div class="panel-body">
						<div class="images-wrap">
							<div class="row" id="product-upload-gallery-images-wrap"></div>
						</div>
						<div class="text-center">
							<button id="product-upload-gallery-submit" type="button" class="btn btn-primary">Adicionar imagem</button>
							<?= $this->Form->input('gallery-input-data', [
								'type' => 'hidden',
								'id' => 'product-upload-gallery-input-data'
							]) ?>
						</div>
					</div>
				</div> <!-- Galeria de imagens -->
				<hr />
				<!-- Gerenciamento de opcionais -->
				<div class="panel panel-info" id="product-complement-wrap">
					<div class="panel-heading"><b>Configuração de opcionais</b></div>
					<div class="panel-body">
						<!-- Interface -->
						<product-complement />
						<!-- Interface -->
					</div>
				</div>
				<!-- Gerenciamento de opcionais -->
				<!-- SUbmit -->
				<div class="form-group">
					<button type="submit" class="btn btn-success">Adicionar produto</button>
				</div> <!-- SUbmit -->
			</div>
			<div class="col-sm-4">
				<!-- Panel categoria -->
				<div class="panel panel-info">
					<div class="panel-heading"><b>Categoria</b></div>
					<div class="panel-body">
						<?= $this->Form->input('category', [
							'label' => false,
							'class' => 'form-control',
							'options' => $categoriesOption,
							'id' => 'product-add-category-select'
						]) ?>
						<hr />
						<div id="product-add-category-wrap">
							<div class="form-group">
								<label>Nova categoria</label>
								<input type="text" class="form-control" autocomplete="off" id="product-add-category-input"  />
								<span class="text-danger" id="product-add-category-input-error"></span>
							</div>
							<div class="form-group">
								<?= $this->Form->input(null, [
									'label' => 'Categoria pai',
									'class' => 'form-control',
									'id' => 'product-add-category-select-parent',
									'options' => ['0' => 'Nenhuma'] + $categoriesParent
								]) ?>
							</div>
							<button type="button" id="product-add-category-submit" class="btn btn-sm btn-primary pull-right">Nova categoria</button>
						</div>
					</div>
				</div> <!-- Panel categoria -->
				<!-- Panel thumb -->
				<div class="panel panel-info" id="product-panel-thumb-wrap" data-upload-image-simple='{"type": "product"}'>
					<div class="panel-heading"><b>Imagem de capa</b></div>
					<div upload-image class="img-wrap"></div>
					<div class="text-center">
						<button type="button" id="product-thumb-upload-submit" upload-submit class="btn btn-primary">Carregar imagem</button>
						<?= $this->Form->input('thumb-input-data', [
							'type' => 'hidden',
							'upload-input-data' => true
						]) ?>
					</div>
				</div>
				<!-- Panel thumb -->
			</div>
		</div>
		<?= $this->Form->end() ?>
	</div>
</section>