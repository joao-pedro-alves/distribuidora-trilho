<?php
$this->layout('panel');
$this->assign('crumb-panel', 'categories');
?>
<section id="panel-products-manager">
	<h1 class="main-page-title">
		<?php if ($this->request->action == 'categoryAdd'): ?>
		Adicionando categoria
		<?php else: ?>
		Alterando categoria
		<?php endif ?>
	</h1>
	<div class="content-padding-wrap">
		<!-- Formulário -->
		<?= $this->Form->create($categoryEntity) ?>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group">
					<?= $this->Form->input('name', [
						'label' => 'Nome',
						'class' => 'form-control'
					]) ?>	
				</div>
				<div class="form-group">
					<?= $this->Form->input('parent_id', [
						'label' => 'Categoria pai',
						'class' => 'form-control',
						'options' => ['0' => 'Nenhuma'] + $categoriesOption,
						'id' => 'product-add-category-select'
					]) ?>	
				</div>	
				<button class="btn btn-success">Salvar</button>
				<?php if ($this->request->action == 'categoryEdit'): ?>
				<div class="pull-right">
					<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'categoryDelete', $categoryEntity->id]) ?>" class="btn btn-danger" onclick="return confirm('Tem certeza que deseja exclui esta categoria PERMANENTEMENTE?')">Excluir</a>
				</div>
				<?php endif ?>
			</div>
		</div>
		<?= $this->Form->end() ?>
		<!-- Formulário -->
	</div>
</section>