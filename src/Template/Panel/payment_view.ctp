<?php 
	$this->layout('panel');
	$this->assign('crumb-panel', 'payments');
?>

<section id="page-payment-view">
	<h1 class="main-page-title">Visualizando fatura</h1>
	<div class="content-padding-wrap">
	<!-- Mensagem de status -->
	<?php if ($this->request->query('action') == 'status' && $this->request->query('status') == 'success'): ?>
	<div class="alert alert-success">
		<span><i class="fa fa-check"></i> <b>Fatura editada com sucesso!</b></span>
	</div>
	<?php endif ?>
	<!-- Mensagem de status -->
	<?= $this->Form->create($paymentEntity) ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="text-center">
				<div><b>Marcar como:</b></div>
				<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentSet', $paymentEntity->id, 1]) ?>" class="btn btn-lg btn-success">
					<i class="fa fa-thumbs-up"></i> Pago
				</a>
				<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentSet', $paymentEntity->id, 2]) ?>" class="btn btn-lg btn-danger"><i class="fa fa-thumbs-down"></i> Cancelado</a>
			</div>
			<hr />
			<h1>
				<?php if ($paymentEntity->status == 0): ?>
				<span class="label label-warning">Pendente</span>
				<?php elseif($paymentEntity->status == 1): ?>
				<span class="label label-success">Aprovado</span>
				<?php else: ?>
				<span class="label label-danger">Cancelado</span>
				<?php endif ?>
			</h1>
			<hr />
			<h2>Cliente</h2>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<b>Nome</b>	
						<div><?= $paymentEntity->user->full_name ?></div>
					</div>
					<div class="div">
						<b>CPF / CNPJ</b>	
						<div><?= $paymentEntity->user->document ?></div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<b>Endereço</b>	
						<div><?= $paymentEntity->user->address_street ?>, <?= $paymentEntity->user->address_neighborhood ?></div>
						<div><?= $paymentEntity->user->address_city ?> - <?= $paymentEntity->user->address_state ?>, <?= $paymentEntity->user->zipcode ?></div>
					</div>
					<div class="div">
						<b>Email</b>	
						<div><?= $paymentEntity->user->email ?></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-12">
			<hr />
		</div>

		<div class="col-sm-12">
			<h2>Fatura</h2>
			<div class="row">
				<div class="col-md-6">
					<b>Data</b>	
					<div><?= $paymentEntity->created->format('d/m/Y') ?></div>
				</div>
				<div class="col-md-6">
					<b>Valor</b>	
					<div>R$<?= number_format($paymentEntity->amount, 2, ',', '.') ?></div>
				</div>
				<div class="col-md-12">
					<hr />
					<panel class="panel-info">
						<div class="panel-heading"><b>Produtos</b></div>
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Nome</th>
									<th>Tamanho</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($paymentEntity->items as $item): ?>
								<tr>
									<td><?= $item->product->id ?></td>
									<td>
										<strong><?= $item->product->name ?></strong>
										<ul>
											<?php foreach ($item->complements as $complement): ?>
											<li><i><?= $complement->complement->name ?> - <?= sprintf("%.3fm", $complement->size) ?></i></li>
											<?php endforeach ?>
										</ul>
									</td>
									<td><?= sprintf("%.3fm", $item->size) ?></td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</panel>
				</div>
			</div>
		</div>
	</div>
	<?= $this->Form->end() ?>
	</div>
</section>