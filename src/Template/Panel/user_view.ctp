<?php $this->layout(null) ?>
<section id="panel-user-view">
	<!-- Painel de confirmação -->
	<?php if ($user->status === 0): ?>
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="panel-title"><b>Usuário com aprovação pendente</b></div>
		</div>
		<div class="panel-body">
			<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userApprove', $user->id]) ?>" class="btn btn-info" onclick="return confirm('Tem certeza que deseja APROVAR o cadastro deste usuário?')">
				<i class="fa fa-thumbs-up"></i> Aprovar cadastro
			</a>
			<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userDisapprove', $user->id]) ?>" class="btn btn-warning" onclick="return confirm('Tem certeza que deseja DESAPROVAR o cadastro deste usuário?')">
				<i class="fa fa-thumbs-down"></i> Desaprovar cadastro
			</a>
		</div>
	</div>
	<?php endif ?> 
	<!-- Painel de confirmação -->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<b>Nome</b>
				<div><?= $user->full_name ?></div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<b>Sobrenome</b>
				<div><?= $user->last_name ?></div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<b>CEP</b>
				<div><?= $user->zipcode ?></div>
			</div>
			<div class="form-group">
				<b>Endereço</b>
				<div><?= $user->address_street ?></div>
			</div>	
			<div class="form-group">
				<b>Número</b>
				<div><?= $user->address_number ?></div>
			</div>	
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<b>Bairro</b>
				<div><?= $user->address_neighborhood ?></div>
			</div>	
			<div class="form-group">
				<b>Estado</b>
				<div><?= $user->address_state ?></div>
			</div>	
			<div class="form-group">
				<b>Cidade</b>
				<div><?= $user->address_city ?></div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<b>Email</b>
				<div><?= $user->email ?></div>
			</div>	
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<b>Telefone</b>
				<div><?= $user->phone ?></div>
			</div>	
		</div>
	</div>
</section>