<?php
$this->layout('panel');
$this->assign('crumb-panel', 'categories');
?>
<section id="panel-products-manager">
	<h1 class="main-page-title"><i class="fa fa-reorder"></i> Categorias</h1>
	<div class="content-padding-wrap">
		<div class="form-group text-right">
			<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'categoryAdd']) ?>" class="btn btn-success"><i class="fa fa-plus"></i> Nova categoria</a>
		</div>
		<!-- Lista de categorias -->
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<!-- Mensagem de sucesso -->
				<?php if ($this->request->query('status') == 'success'): ?>
				<div class="alert alert-success">
					<!-- Adicionando -->
					<?php if ($this->request->query('action') == 'add'): ?>
					<p><i class="fa fa-check"></i> Categoria adicionada com sucesso</p>
					<?php endif ?>
					<!-- Adicionando -->
					<!-- Editando -->
					<?php if ($this->request->query('action') == 'edit'): ?>
					<p><i class="fa fa-check"></i> Categoria editada com sucesso</p>
					<?php endif ?>
					<!-- Editando -->
					<!-- Excluindo -->
					<?php if ($this->request->query('action') == 'delete'): ?>
					<p><i class="fa fa-check"></i> Categoria excluída com sucesso</p>
					<?php endif ?>
					<!-- Excluindo -->
				</div>
				<?php endif ?>
				<!-- Mensagem de sucesso -->
				<table class="table table-striped table-bordered body-middle text-centered">
					<thead>
						<tr>
							<th></th>
							<th width="250" align="center">Nome</th>
							<th>Sub-Categorias</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($categories as $category): ?>
						<tr>
							<td>
								<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'categoryEdit', $category->id]) ?>" class="btn btn-xs btn-success"><i class="fa fa-cog"></i> Editar</a>
							</td>
							<td><strong><?= $category->name ?></strong></td>
							<td>
								<?php foreach ($category->child_categories as $child): ?>
								<p><a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'categoryEdit', $child->id]) ?>""><?= $child->name ?></a></p>
								<?php endforeach ?>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- Lista de categorias -->
	</div>
</section>