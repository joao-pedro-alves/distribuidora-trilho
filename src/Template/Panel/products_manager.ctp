<?php
	$this->layout('panel');
	$this->assign('crumb-panel', 'products');
?>
<section id="panel-products-manager">
	<h1 class="main-page-title"><i class="fa fa-shopping-cart"></i> Produtos</h1>
	<div class="content-padding-wrap">
		<div class="form-group text-right">
			<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'productAdd']) ?>" class="btn btn-success"><i class="fa fa-plus"></i> Novo produto</a>
		</div>
		<!-- Busca de usuários -->
		<?= $this->Element('Panel/search-input', [
			'placeholder' => 'Buscar produto'
		]) ?>
		<!-- Busca de usuários -->

		<!-- Lista de produtos -->
		<table class="table body-middle table-striped">
			<thead>
				<tr>
					<th width="115"></th>
					<th width="80"></th>
					<th>#</th>
					<th>Nome</th>
					<th>Categoria</th>
					<th>Valor</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($products as $product): ?>
				<tr>
					<td valign="middle">
						<div class="btn-group">
							<button 
								class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-cogs"></i> Opções <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="#" ><i class="fa fa-cog"></i> Editar</a>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<img width="40" height="40" src="<?= $this->Url->build('/uploads/' . $product->thumb) ?>">
					</td>
					<td valign="middle"><?= $product->id ?></td>
					<td valign="middle"><?= $product->name ?></td>
					<td valign="middle"><?= $product->category->name ?></td>
					<td valign="middle"><?= $product->price_money_format ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<!-- Lista de produtos -->

		<!-- Paginação -->
		<?= $this->Element('Panel/pagination', [
			'hashtag' => '#'
		]) ?>
		<!-- Paginação -->
	</div>
</section>