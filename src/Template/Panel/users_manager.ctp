<?php $this->layout('panel') ?>
<section id="panel-users-manager">
	<h1 class="main-page-title"><i class="fa fa-users"></i> Usuários</h1>
	<div class="content-padding-wrap">
		<!-- Mensagem de sucesso usuário deletado -->
		<?php if ($this->request->query('status') == 'user-deleted'): ?>
		<div class="alert alert-warning">
			<i class="fa fa-warning"></i> Usuário deletado com sucesso
		</div>
		<?php endif ?>
		<!-- Mensagem de sucesso usuário deletado -->
		<!-- Usuários em aprovação -->
		<div class="panel panel-info unapproved-users">
			<div class="panel-heading">
				<b><i class="fa fa-clock-o"></i> Existem <?= $usersPending->count() ?> usuários aguardando aprovação</b>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th width="270">Ações</th>
						<th>Nome</th>
						<th>Cidade - Estado</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($usersPending as $user): ?>
					<tr>
						<td>
							<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userEdit', $user->id]) ?>" class="btn btn-xs btn-primary">
								<i class="fa fa-cog"></i> Editar
							</a>
							<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userView', $user->id]) ?>" data-featherlight="ajax" class="btn btn-xs btn-success">
								<i class="fa fa-eye"></i> Visualizar
							</a>
							<div class="btn-group">
								<button class="btn btn-xs btn-defaul dropdown-toggle" data-toggle="dropdown">
									Aprovação <i class="fa fa-caret-down"></i>
								</button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userApprove', $user->id]) ?>" onclick="return confirm('Você tem certeza que deseja APROVAR este cadastro?')">
											<b class="text-success"><i class="fa fa-check"></i> Aprovar</b>
										</a>
									</li>
									<li>
										<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userDisapprove', $user->id]) ?>" onclick="return confirm('Você tem certeza que deseja DESAPROVAR este cadastro?')">
											<b class="text-danger"><i class="fa fa-close"></i> Recusar</b>
										</a>
									</li>
								</ul>
							</div>
						</td>
						<td><?= $user->full_name ?></td>
						<td><?= $user->address_city ?> - <?= $user->address_state ?></td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div> <!-- Usuários em aprovação -->
	
		<!-- Busca de usuários -->
		<?= $this->Element('Panel/search-input', [
			'placeholder' => 'Buscar usuário'
		]) ?>
		<!-- Busca de usuários -->

		<!-- Lista de usuários -->
		<?php if ($this->request->query('q')): ?>
		<div class="alert alert-success">
			<span>Foram encontrados <b><?= $this->Paginator->params()['count'] ?></b> resultados para sua busca</span>
		</div>
		<?php endif ?>
		<table class="table table-striped" id="approved-users">
			<thead>
				<tr>
					<th width="105">Ações</th>
					<th>Nome</th>
					<th>Cidade</th>
					<th>Email</th>
					<th>Telefone</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($usersAllowed as $user): ?>
				<tr>
					<td>
						<div class="btn-group">
							<button 
								class="btn btn-xs btn-primary dropdown-toggle"
								data-toggle="dropdown"
							>
								<i class="fa fa-cogs"></i> Opções <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userEdit', $user->id]) ?>" ><i class="fa fa-cog"></i> Editar</a>
								</li>
								<li>
									<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'userView', $user->id]) ?>" data-featherlight="ajax">
										<i class="fa fa-eye"></i> Visualizar
								</li>
							</ul>
						</div>
					</td>
					<td><?= $user->full_name ?></td>
					<td><?= $user->address_city ?> - <?= $user->address_state ?></td>
					<td><?= $user->email ?></td>
					<td><?= $user->phone ?></td>
					<td align="center">
						<?php if ($user->status_name == 'Aprovado'): ?>
						<span title="Cadastro aprovado" class="label label-success"><i class="fa fa-check"></i></span>
						<?php endif ?>
						<?php if ($user->status_name == 'Desaprovado'): ?>
						<span title="Cadastro desaprovado" class="label label-danger"><i class="fa fa-close"></i></span>
						<?php endif ?>
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		<!-- Paginação -->
		<?= $this->Element('Panel/pagination', [
			'hashtag' => 'approved-users'
		]) ?>
		<!-- Paginação -->
		<!-- Lista de usuários -->
	</div>
</section>