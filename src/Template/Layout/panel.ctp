<!DOCTYPE html>
<html id="layout-panel">
<head>
	<meta charset="UTF-8">
	<title>Distribuidora Trilho - Panel</title>
	<?= $this->Element('styles') ?>
</head>
<body ng-app="panel">
	<test></test>
	<div id="global-wrap">
		<!-- Conteúdo global -->
		<div id="global-content">
			<div class="row col">
				<div class="col-sm-4 col-md-3 col no-margin">
					<div id="main-menu">
						<div class="brand">Distribuidora Trilho</div>
						<ul class="nav nav-pills nav-stacked">
							<li class="<?= ($this->request->action == 'dashboard') ? 'active' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> Dashboard</a></li>
							<li class="<?= ($this->fetch('crumb-panel') == 'products') ? 'active' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'productsManager']) ?>"><i class="fa fa-shopping-cart"></i> Produtos</a></li>
							<li class="<?= ($this->fetch('crumb-panel') == 'categories') ? 'active' : '' ?>">
								<a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'categoriesManager']) ?>"><i class="fa fa-reorder"></i> Categorias</a>
							</li>
							<li class="<?= ($this->fetch('crumb-panel') == 'payments') ? 'active' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'paymentsManager']) ?>"><i class="fa fa-money"></i> Faturas</a></li>
							<li class="<?= (in_array($this->request->action, ['usersManager', 'userEdit'])) ? 'active' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'usersManager']) ?>"><i class="fa fa-group"></i> Usuários</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-8 col-md-9 col no-margin">
					<nav id="top-menu">
						<ul class="nav nav-pills pull-left">
							<li><a href="#"><i class="fa fa-plus"></i> Novo produto</a></li>
							<li><a href="#"><i class="fa fa-plus"></i> Nova categoria</a></li>
							<li><a href="#"><i class="fa fa-plus"></i> Novo opcional</a></li>
						</ul>
						<ul class="nav nav-pills pull-right">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									<b><i class="fa fa-user"></i> <?= @$auth['full_name'] ?></b> <span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?= $this->Url->build(['controller' => 'Site', 'action' => 'dashboard']) ?>">Ir para o site</a></li>
									<li><a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'logout']) ?>">Sair da conta</a></li>
								</ul>
							</li>
						</ul>
					</nav>
					<div id="page-content">
					<?= $this->fetch('content') ?>
					</div>
					<div id="global-footer">
						<span>Distribuidora Trilho - Todos os direitos reservados</span>
					</div>
				</div>
			</div>			
		</div> <!-- Conteúdo global -->
	</div>
	<?= $this->Element('scripts') ?>
</body>
</html>