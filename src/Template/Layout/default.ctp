<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Distribuidora Trilho<?= $this->fetch('title') ?></title>
    <?= $this->Element('styles') ?>
</head>
<body>
   <div id="global-wrap">
       <header id="global-header"></header>
       <section id="global-content">
           <?= $this->fetch('content') ?>
       </section>
       <footer id="global-footer"></footer>
   </div> 
   <?= $this->Element('scripts') ?>
</body>
</html>