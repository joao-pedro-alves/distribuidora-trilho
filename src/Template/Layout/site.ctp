<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Distribuidora Trilho</title>
	<?= $this->Element('styles') ?>
</head>
<body>
	<!-- Menu principal -->
	<div id="global-header">
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Distribuidora Trilho</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Início</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categoria <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Teste</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categoria <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Teste</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categoria <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Teste</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categoria <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Teste</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><b><?= @$auth['full_name'] ?></b> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php if ($auth['role'] == 1): ?>
								<li><a href="<?= $this->Url->build(['controller' => 'Panel', 'action' => 'dashboard']) ?>">Painel Administrativo</a></li>
								<li role="separator" class="divider"></li>
								<?php endif ?>
								<li><a href="#">Editar conta</a></li>
								<li><a href="#">Meus orçamentos</a></li>
								<li><a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'logout']) ?>">Sair</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</div>
	<!-- Menu principal -->
	<div id="global-content">
		<div class="container">
			<?= $this->fetch('content') ?>
		</div>
	</div>
	<div id="global-footer"></div>
	<?= $this->Element('scripts') ?>
</body>
</html>