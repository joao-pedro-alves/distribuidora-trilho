<section id="users-register-success" class="smt">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="alert alert-success">
				<h3><i class="fa fa-check"></i> Cadastro enviado para aprovação</h3>
				<p>Seu cadastro foi feito com sucesso e já foi enviado para nossa equipe analisa-lo. Notificaremos você através do email <b><?= $email ?></b> assim que sua conta for ativada.</p>
				<p><i>Agradecemos o seu cadastro.</i></p>
			</div>		
		</div>
	</div>
</section>