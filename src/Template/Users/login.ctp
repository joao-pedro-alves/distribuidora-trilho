<?php $this->assign('title', ' ') ?>

<section id="users-login">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<?php if ($this->request->query('unauthorized')): ?>
				<div class="alert alert-danger">
					Usuário ou senha inválidos
				</div>
				<?php endif ?>

				<?php if ($this->request->query('status') == 'denied'): ?>
				<div class="alert alert-danger">
					Sua conta ainda não está ativada
				</div>
				<?php endif ?>
				
				<form action="#" method="post">
					<div class="form-group">
						<label>Email</label>
						<input name="email" type="text" class="form-control" />
					</div>
					<div class="form-group">
						<label>Senha</label>
						<input name="password" type="password" class="form-control" />	
					</div>
					<div class="form-group">
						<button class="btn btn-success">Acessar</button>
						<div class="pull-right">
							<a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'register']) ?>" class="btn">Criar conta</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</section>
