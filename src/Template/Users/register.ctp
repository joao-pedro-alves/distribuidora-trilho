<section id="users-register" class="section-top-margin">
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<?php if (isset($formErrors)): ?>
			<div class="alert alert-danger">
				<b>Verifique se os dados foram corretamente preenchidos</b>
			</div>
			<?php endif ?>
			<?= $this->Form->create($userEntity) ?>
			<div class="form-group">
				<?= $this->Form->input('first_name', [
					'label' => 'Nome',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="form-group">
				<?= $this->Form->input('last_name', [
					'label' => 'Sobrenome',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="form-group">
				<?= $this->Form->input('document', [
					'label' => 'CPF / CNPJ',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<?= $this->Form->input('zipcode', [
							'label' => 'CEP',
							'class' => 'form-control'
						]) ?>
					</div>	
					<div class="form-group">
						<?= $this->Form->input('address_number', [
							'label' => 'Número',
							'class' => 'form-control'
						]) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->input('address_city', [
							'label' => 'Cidade',
							'class' => 'form-control'
						]) ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<?= $this->Form->input('address_street', [
							'label' => 'Endereço',
							'class' => 'form-control'
						]) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->input('address_neighborhood', [
							'label' => 'Bairro',
							'class' => 'form-control'
						]) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->input('address_state', [
							'label' => 'Estado',
							'class' => 'form-control'
						]) ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<?= $this->Form->input('phone', [
					'label' => 'Telefone',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="form-group">
				<?= $this->Form->input('email', [
					'label' => 'Email',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="form-group">
				<?= $this->Form->input('password', [
					'label' => 'Senha',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="form-group">
				<?= $this->Form->input('password_confirm', [
					'type' => 'password',
					'label' => 'Confirmar senha',
					'class' => 'form-control'
				]) ?>
			</div>
			<div class="form-group">
				<button class="btn btn-success">Cadastrar</button>
			</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</section>